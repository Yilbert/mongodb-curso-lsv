
from bson.objectid import ObjectId
from car.databases.client import MongoLsv # noqa



mongo_lsv = MongoLsv()

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="act_lsv",
        collection="business",
        record={
            "name": "Desarrollo de Software",
            "num_employed":"115",
            "business_type": "TypeBusiness", 
            "created_at": "2022-03-01",
            "modified_at": "2022-05-02", },
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="act_lsv",collection="business"
    )
)

"""
this file is to create a new record in collection business 
"""