
from bson.objectid import ObjectId 
from car.databases.client import MongoLsv


mongo_lsv = MongoLsv()

print(
    mongo_lsv.update_record_in_collection(
        db_name="act_lsv",
        collection="business",
        record_query={"_id": ObjectId("62b1e9b41ed3d4e5f7080332")},
        record_new_value={"name": "Telecom S.A"},
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="act_lsv", collection="business"
    )
)

"""
This file is to update the record in collection business
"""