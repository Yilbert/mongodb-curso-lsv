from dataclasses import asdict, dataclass
from datetime import datetime



@dataclass
class TypeBusiness:
    uuid: str  # UUID
    name: str  

    b={"name" :"telecomunicaciones"}

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class Business:
    uuid: str
    name: str
    num_employed: str
    business_type: TypeBusiness
    created_at: datetime
    modified_at: datetime


    def to_dict(self) -> dict:
        return asdict(self)


    t={
       "name": "radio y tv",
       "num_employed":"34",
       "business_type": "TypeBusiness", 
       "created_at": "2020-08-01",
       "modified_at": "2020-08-02",}

    @property
    def get_typebusines(self):
        return self.business.name
